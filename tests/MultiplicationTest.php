<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/City.php");
#Test unitaire pour tester la fonction de multiplication
class MultiplicationTest extends \PHPUnit\Framework\TestCase
{
   public function testMultiply()
   {
        $this->assertEquals(4, multiply(2, 2));
   }
}