*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
Go to home page
    Wait Until Page Contains    CREATIVE

Check title
    ${title} =    Get Title
    Should Be Equal    ${title}    Rage Freebie HTML5 Landing page
